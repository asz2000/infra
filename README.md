# skillbox-diploma
Подготовка инфраструктуры для докер-сервиса в Amazon (AWS)

##  Описание работы
Разворачивает instance в AWS. 
Устанавливает сервис docker
Создает Application Load Balancer и Hosted Zone для указанного домена в сервисе Route53
Доступ с указанного домена по порту 80 перенаправляется в docker сервис на порт 8080

## Переменные для инфраструктуры
Перед запуском terraform необходимо указать(проверить) переменные в файле variables.tf
 
 "region"  - указать регион AWS

 "domain" - имя зарегистрированного домена, в котором нужно указать name_server

 "key_name" - имя ключа для доступа к instance

 "ssh_key_private" - расположение ключа для доступа к instance

## Запуск скриптов

1. Запускаем:
```shell
  terraform init
  terraform plan
  terraform apply
```
2. После успешного выполнения будет подготовлен файл ansible/hosts 
    и выведен список name серверов для записи их в сервисе регистрации домена (freenom.com)
3. В файле ansible/vars.yaml указать учетные данные для доступа к dockerhub.
   ```shell
      username: 
      password:
   ```
4. В каталоге ansible запустить сценарий:
   ```shell
      ansible-playbook docker.yaml -b
      ansible-playbook deploy_docker.yaml -b
   ```
5. Приложение будет доступно по адресу: http://<domain(по умолчанию devops-cert.tk)>