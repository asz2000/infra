# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  region = var.region
}


# Получить ID VPC-подсетей из источника данных aws_vpc.
data "aws_subnet_ids" "default" {
    vpc_id = data.aws_vpc.default.id
}

# Найти в VPC-подсетях Default VPC.
data "aws_vpc" "default" {
default = true
}


# По умолчанию все AWS-ресурсы, включая ALB, запрещают любой входящий/исходящий трафик. 
# Поэтому настроим группу безопасности, которая разрешит входящий трафик на 80 порт ресурса ALB и исходящий на любой порт этого же ресурса.

resource "aws_security_group" "alb" {
    name = "terraform-alb-security-group"

    # Разрешить входящие HTTP
    ingress {
    from_port        = 80
    to_port            = 80
    protocol        = "tcp"
    cidr_blocks        = ["0.0.0.0/0"]
    }

    # Разрешить все исходящие
    egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }
}

# Эта группа безопасности применяется к ресурсу aws_launch_configuration
resource "aws_security_group" "instance" {
  name = "terraform-instance-security-group"

  dynamic ingress {
      for_each = ["80", "22", "8080"]
        content {
        from_port   = ingress.value
        to_port     = ingress.value
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        }
    }
    
    # Разрешить все исходящие
    egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }
}


#############################################################################
# ALB (Application Load Balancer)
#############################################################################


# alb — имя ресурса.
# name — имя балансировщика.
# load_balancer_type — тип балансировщика.
# subnets — имя VPC-подсети. В этом случае подсеть указана как default. 
# К сведению. По умолчанию при регистрации в AWS для всех регионов автоматически 
# создаются подсети с именем default.
# security_groups — имя группы безопасности, которую создали выше.

resource "aws_lb" "alb" {
    name = "terraform-alb"
    load_balancer_type = "application"
    subnets = data.aws_subnet_ids.default.ids
    security_groups = [aws_security_group.alb.id]
}

# Создание listener
# http — имя ресурса прослушивателя.
# load_balancer_arn — имя ресурса вышесозданного ALB. В нашем случае имя alb.

resource "aws_lb_listener" "http" {
    load_balancer_arn = aws_lb.alb.arn
    port = 80
    protocol = "HTTP"

# Страница 404 если будут запросы, которые не соответствуют никаким правилам прослушивателя.
    default_action {
        type = "fixed-response"
        fixed_response {
        content_type = "text/plain"
        message_body = "404: страница не найдена"
        status_code = 404
        }
    }
}

# Включаем правило прослушивателя, которое отправляет запросы,
# соответствующие любому пути, в целевую группу для ASG.
resource "aws_lb_listener_rule" "asg-listener_rule" {
    listener_arn    = aws_lb_listener.http.arn
    priority        = 100
    
    condition {
        path_pattern {
        values  = ["*"]
        }
    }
    
    action {
        type = "forward"
        target_group_arn = aws_lb_target_group.asg-target-group.arn
    }
}

# Создаём целевую группу aws_lb_target_group для ASG.
# Каждые 15 сек. будут отправляться HTTP запросы и если ответ 200, то все ОК, иначе
# произойдет переключение на доступный инстанс. 
resource "aws_lb_target_group" "asg-target-group" {
    name = "terraform-aws-lb-target-group"
    port = 8080
    protocol = "HTTP"
    vpc_id = data.aws_vpc.default.id
    target_type = "instance"
    health_check {
        path                = "/health"
        protocol            = "HTTP"
        matcher             = "200"
        interval            = 15
        timeout             = 3
        healthy_threshold   = 2
        unhealthy_threshold = 2
    }
}


resource "aws_instance" "ubuntu-ec2" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.instance.id]
  #user_data = file("user_data.sh")
  key_name = "${var.key_name}"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Docker Service"
    Env = "Production"
    Tier = "Frontend"
    CM = "Ansible"
  }
  
  lifecycle {
    create_before_destroy = true
  }

}


# Ищем образ с последней версией Ubuntu
# !!! Здесь лучше найти образ, с установленным сервисом Docker, но по заданию установка сервиса через Ansible !!!
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

resource "aws_lb_target_group_attachment" "asg-target-group" {
  target_group_arn = aws_lb_target_group.asg-target-group.arn
  target_id        = aws_instance.ubuntu-ec2.id
  port             = 8080
}

resource "aws_eip" "eip-bastion" {
  instance = aws_instance.ubuntu-ec2.id
  tags = {
    Name  = "Server IP for Ansible"
  }
}


resource "aws_route53_zone" "my_hosted_zone" {
  name = "${var.domain}"
  force_destroy = false
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.my_hosted_zone.zone_id
  name    = "${var.domain}"
  type    = "A"

  alias {
    name                   = aws_lb.alb.dns_name
    zone_id                = aws_lb.alb.zone_id
    evaluate_target_health = true
  }
}


### The Ansible inventory file
resource "local_file" "AnsibleInventory" {
     content = templatefile("inventory.tmpl", {
           bastion-dns = aws_eip.eip-bastion.public_dns,
           bastion-ip = aws_eip.eip-bastion.public_ip,  
           bastion-id = aws_instance.ubuntu-ec2.id
           bastion_user="ubuntu"
           bastion_key="${var.ssh_key_private}"
           #private-dns = aws_instance.i-private.*.private_dns,
           #private-ip = aws_instance.i-private.*.private_ip,
           #private-id = aws_instance.i-private.*.id 
           } ) 
          filename = "ansible/hosts"
} 

output aws_route53_servernames {
    description = "The name servers route53 zone."
    value = "${join("", aws_route53_zone.my_hosted_zone.name_servers)}"    
}